var gulp            = require('gulp');
var sass            = require('gulp-sass');
var notify          = require('gulp-notify');
var connect         = require('gulp-connect-php');
var cssmin          = require('gulp-cssmin');
var rename          = require('gulp-rename');
var minify          = require('gulp-minify');
var eslint          = require('gulp-eslint');
var gutil           = require('gulp-util');
var ftp             = require('vinyl-ftp');
var browserSync     = require('browser-sync').create();
var serve           = require('gulp-serve');

var config = {
    sassPath: 'app/scss',
    bootstrapDir: '././node_modules/bootstrap/dist/css'
};

gulp.task('sass', function(){
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/css'))
        .pipe(notify({ message: 'CSS ready!' }))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('sass-documentation', function(){
    return gulp.src('documentation/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('documentation/css'))
        .pipe(notify({ message: 'CSS ready!' }))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: 'app'
        },
        port: 9160
    });
    browserSync.notify('Browser bijgewerkt!');
});

gulp.task('browserSync-documentation', function() {
    browserSync.init({
        server: {
            baseDir: 'documentation'
        },
        port: 9160
    });
    browserSync.notify('Browser bijgewerkt!');
});

gulp.task('watch', ['browserSync', 'sass', 'lint'], function(){
    gulp.watch('app/scss/**/*.scss', ['sass']);
    gulp.watch('app/**/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('watch-documentation', ['browserSync-documentation', 'sass-documentation', 'lint-documentation'], function(){
    gulp.watch('documentation/scss/**/*.scss', ['sass']);
    gulp.watch('documentation/css/**/*.css', browserSync.reload);
    gulp.watch('documentation/**/*.html', browserSync.reload);
    gulp.watch('documentation/js/**/*.js', browserSync.reload);
});

gulp.task('php', function() {
    connect.server({ base: 'app', port: 8010, keepalive: true});
});

gulp.task('css-minify', function () {
    gulp.src(['app/css/*.css', '!app/css/*.min.css'])
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css/min'))
        .pipe(notify({ message: 'CSS minified!' }));
});

gulp.task('js-minify', function() {
    gulp.src(['app/js/*.js', '!app/js/*.min.js'])
        .pipe(minify({
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '.min.js']
        }))
        .pipe(gulp.dest('app/js/min'))
        .pipe(notify({ message: 'JS minified!' }));
});

gulp.task('lint', function() {
    return gulp.src('app/js/**').pipe(eslint({
        'rules':{
            'quotes': [1, 'single'],
            'semi': [1, 'always']
        }
    }))
        .pipe(eslint.format())
        .pipe(eslint.failOnError());
});

gulp.task('lint-documentation', function() {
    return gulp.src('documentation/js/**').pipe(eslint({
        'rules':{
            'quotes': [1, 'single'],
            'semi': [1, 'always']
        }
    }))
        .pipe(eslint.format())
        .pipe(eslint.failOnError());
});

gulp.task('lint-css', function lintCssTask() {
    const gulpStylelint = require('gulp-stylelint');

    return gulp
        .src('app/scss/*.scss')
        .pipe(gulpStylelint({
            reporters: [
                {formatter: 'string', console: true}
            ]
        }));
});

gulp.task('build-production', ['lint', 'js-minify', 'css-minify'], function() {
    console.log('Building for production');
});

gulp.task('build', ['lint', 'sass'], function () {
    console.log('Building for deployment');
});

gulp.task('test', ['lint', 'lint-css'], function () {
    console.log('Linting completed!');
});

gulp.task('serve', serve('app'));